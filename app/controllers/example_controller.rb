# frozen_string_literal: true

module Handaluj
  class ExampleController < ApplicationController
    def index
      render json: "Hello World!!!"
    end
  end
end

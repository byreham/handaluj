# frozen_string_literal: true

require "handaluj/rails/routes/mapping"
require "handaluj/rails/routes/mapper"

module Handaluj
  module Rails
    class Routes
      module Helper
        def use_handaluj(options = {}, &block)
          Handaluj::Rails::Routes.new(self, &block).generate_routes!(options)
        end
      end

      def self.install!
        ActionDispatch::Routing::Mapper.include Handaluj::Rails::Routes::Helper
      end

      attr_reader :routes

      def initialize(routes, mapper = Mapper.new, &block)
        @routes = routes
        @mapping = mapper.map(&block)
      end

      def generate_routes!(_options = {})
        routes.get :example, to: "example#index"
      end
    end
  end
end

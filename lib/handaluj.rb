# frozen_string_literal: true

require_relative "handaluj/version"
require_relative "handaluj/engine"

# require "rails"
# require "active_support/core_ext/numeric/time"
# require "active_support/dependencies"
# require "orm_adapter"
# require "set"

# module Devise
#   autoload :FailureApp, 'devise/failure_app'
#   autoload :OmniAuth, 'devise/omniauth'
#   autoload :PathChecker, 'devise/path_checker'
#   autoload :Schema, 'devise/schema'
#   autoload :TestHelpers, 'devise/test_helpers'

module Handaluj
  class Error < StandardError; end

  # module Controllers
  # autoload :Helpers, 'devise/controllers/helpers'
  # autoload :InternalHelpers, 'devise/controllers/internal_helpers'
  # autoload :Rememberable, 'devise/controllers/rememberable'
  # autoload :ScopedViews, 'devise/controllers/scoped_views'
  # autoload :UrlHelpers, 'devise/controllers/url_helpers'
  # end

  def self.setup
    yield self if block_given?
  end

  module Rails
    autoload :Routes, "handaluj/rails/routes"
  end

  require "devise/rails"
end
